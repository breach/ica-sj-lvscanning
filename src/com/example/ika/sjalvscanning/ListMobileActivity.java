package com.example.ika.sjalvscanning;

import android.app.ListActivity;
import android.content.Context;
import android.widget.ListView;
import android.widget.Toast;
import android.view.View;

public class ListMobileActivity extends ListActivity {

	public Context cont;
	static final String[] MOBILE_OS = new String[] { "Korv", "Banan", "Mjölk",
			"Sylt" };

	public ListMobileActivity(Context context) {

		cont = context;
		setListAdapter(new MobileArrayAdapter(cont, MOBILE_OS));

	}
//
//	@Override
//	public void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//
//	}

//	@Override
//	protected void onListItemClick(ListView l, View v, int position, long id) {
//
//		// get selected items
//		String selectedValue = (String) getListAdapter().getItem(position);
//		Toast.makeText(this, selectedValue, Toast.LENGTH_SHORT).show();
//
//	}

}