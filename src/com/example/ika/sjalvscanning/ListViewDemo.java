package com.example.ika.sjalvscanning;

import java.util.ArrayList;
import android.app.ListActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;

public class ListViewDemo extends ListActivity {
	// LIST OF ARRAY STRINGS WHICH WILL SERVE AS LIST ITEMS
	ArrayList<String> listItems = new ArrayList<String>();
	public View view;
	// DEFINING STRING ADAPTER WHICH WILL HANDLE DATA OF LISTVIEW
	ArrayAdapter<String> adapter;

	// RECORDING HOW MUCH TIMES BUTTON WAS CLICKED
	int clickCounter = 0;

	public void onCreate(Bundle icicle, LayoutInflater inflater,
			ViewGroup container) {
		super.onCreate(icicle);
		Button btn = (Button) view.findViewById(R.id.buttn1);

		adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, listItems);
		setListAdapter(adapter);
		view = inflater.inflate(R.layout.hem, container, false);
	}

	// METHOD WHICH WILL HANDLE DYNAMIC INSERTION
	public void addItems() {
		listItems.add("Clicked : " + clickCounter++);
		adapter.notifyDataSetChanged();
	}

	public View getView() {
		return view;

	}
}