package com.example.ika.sjalvscanning;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

public class ReceptFragment extends Fragment {
	public ReceptFragment() {
	}

	public static ImageButton imgB1, imgB2, imgB3, imgB4, imgB5, imgB6;
	public static final String ARG_SECTION_NUMBER = "section_number";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.recept, container, false);
		imgB1 = (ImageButton) view.findViewById(R.id.imgB1);
		imgB2 = (ImageButton) view.findViewById(R.id.imgB2);
		imgB3 = (ImageButton) view.findViewById(R.id.imgB3);

		imgB4 = (ImageButton) view.findViewById(R.id.imgB4);

		imgB5 = (ImageButton) view.findViewById(R.id.imgB5);
		imgB6 = (ImageButton) view.findViewById(R.id.imgB6);

		hemActions();

		return view;
	}

	public void hemActions() {

	}

}