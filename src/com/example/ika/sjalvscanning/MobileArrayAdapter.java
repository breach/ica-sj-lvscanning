package com.example.ika.sjalvscanning;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
 
public class MobileArrayAdapter extends ArrayAdapter<String> {
	private final Context context;
	private final String[] values;
 
	public MobileArrayAdapter(Context context, String[] values) {
		super(context, R.layout.listview, values);
		this.context = context;
		this.values = values;
	}
 
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
			.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
 
		View rowView = inflater.inflate(R.layout.listview, parent, false);
		TextView textView1 = (TextView) rowView.findViewById(R.id.list1);
		TextView textView2 = (TextView) rowView.findViewById(R.id.list2);
		textView1.setText(values[position]);
		textView1.setText(values[position]);

		// Change icon based on name
		String s = values[position];
 
		System.out.println(s);
 
		if (s.equals("Korv")) {
			textView2.setText("10 Kr");
		} else if (s.equals("Banan")) {
			textView2.setText("10 Kr");
		} else if (s.equals("Mjölk")) {
			textView2.setText("10 Kr");
		} else if (s.equals("Sylt")){
			textView2.setText("10 Kr");
		}
 
		return rowView;
	}
}