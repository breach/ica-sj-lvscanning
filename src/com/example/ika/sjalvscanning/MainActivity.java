package com.example.ika.sjalvscanning;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class MainActivity extends FragmentActivity {

	public static Button back;
	SectionsPagerAdapter mSectionsPagerAdapter;

	ViewPager mViewPager;
	protected Activity context;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager());

		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			Fragment fragment;
			switch (position) {
			case 0:
				fragment = new DummySectionFragment1();
				break;
			case 1:
				fragment = new DummySectionFragment2();
				break;
			case 2:
				fragment = new DummySectionFragment3();
				break;

			default:
				fragment = new Fragment();
				break;

			}
			Bundle args = new Bundle();
			fragment.setArguments(args);
			return fragment;
		}

		@Override
		public int getCount() {
			return 3;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			switch (position) {
			case 0:
				return getString(R.string.wizard1);
			case 1:
				return getString(R.string.wizard2);
			case 2:
				return getString(R.string.wizard3);
			}
			return null;
		}
	}

	public static class DummySectionFragment1 extends Fragment {

		public DummySectionFragment1() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			super.onCreateView(inflater, container, savedInstanceState);
			View view = inflater.inflate(R.layout.wizard1, container, false);

			return view;
		}
	}

	public static class DummySectionFragment2 extends Fragment {

		public DummySectionFragment2() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {

			super.onCreateView(inflater, container, savedInstanceState);
			View view = inflater.inflate(R.layout.wizard2, container, false);

			return view;
		}
	}

	public class DummySectionFragment3 extends Fragment {

		public DummySectionFragment3(){};

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {

			super.onCreateView(inflater, container, savedInstanceState);
			View view = inflater.inflate(R.layout.wizard3, container, false);

			back = (Button) view.findViewById(R.id.back);

			kor();
			return view;
		}
	}

	public void kor() {
		
		back.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				MainActivity.this.finish();
			}
		});
	}
}
